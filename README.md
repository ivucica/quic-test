This is a test trying to build github.com/devsisters/goquic Go package,
as well as to use it. Fun stuff!

To build goquic with go 1.6, your binutils may be too old. Tested
to work with Debian package binutils 2.26-8, on Ubuntu 14.04:

    sudo apt-get install binutils=2.26-8

Chrome needs the host whitelisted (still true as of Chrome beta 51),
due to [an apparent security vulnerability][1]:

    google-chrome-beta --quic-host-whitelist=server.hostname

You may also need to enable QUIC itself as well as alternative services.
Either use chrome://flags or:

    google-chrome-beta --quic-host-whitelist=server.hostname \
	    --enable-quic \
		--enable-alternative-services

Observe if QUIC is being used at: chrome://net-internals#quic

To run the reverse proxy for an existing HTTPS service:

    ./quic-reverse_proxy -loglevel 5  -cert /etc/letsencrypt/live/server.hostname/fullchain.pem -key /etc/letsencrypt/live/server.hostname/privkey.pem -port 443 -quic_only https://server.hostname/

You may need to add relevant headers. Example for nginx:

    server {
	    server_name server.hostname;
		...
		...
        add_header Alternate-Protocol 443:quic;
        add_header Alt-Svc 'quic=":443"; ma=2592000; v="33,32,31,30,29,28,27,26,25"';
	    ...
		...
	}

(It's probably okay to skip maximum age, as well as the list of supported quic
versions.)

[1]: https://groups.google.com/a/chromium.org/d/msg/proto-quic/1K79SVSLEiU/bvMLV0juEwAJ
