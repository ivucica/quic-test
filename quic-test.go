package main

import (
	"github.com/devsisters/goquic"
	"log"
)

func main () {
	err := goquic.ListenAndServeQuicSpdyOnly(":8080", "", "", 1, nil)
	log.Fatal(err)
}
