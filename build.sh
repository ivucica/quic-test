#!/bin/bash
set -e

if ! which ninja; then
    echo "No ninja installed. sudo apt-get install ninja-build?"
    exit 1
fi


echo "May require: sudo apt-get install binutils=2.26-8"
echo "Press enter"
read

go get -v -u -d github.com/devsisters/goquic

(
  cd $GOPATH/src/github.com/devsisters/goquic
  ./build_libs.sh
)

#go install github.com/devsisters/goquic



go get -v -u -d github.com/devsisters/goquic/example
go build github.com/devsisters/goquic/example/server.go
go build github.com/devsisters/goquic/example/reverse_proxy.go
